package com.example;

import me.deftware.client.framework.event.Event;
import me.deftware.client.framework.event.events.EventClientCommand;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.wrappers.IChat;

public class Main extends EMCMod {
	
	private EMCModInfo modInfo;
	
	@Override
	public void initialize() {
		modInfo = new EMCModInfo("Example mod", "1");
	}

	@Override
	public EMCModInfo getModInfo() {
		// This is used for the framework to know what mod this is
		return modInfo;
	}

	@Override
	public void onEvent(Event event) {
		// Handle event
	}

}
